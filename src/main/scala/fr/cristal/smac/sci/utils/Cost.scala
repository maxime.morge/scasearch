// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.utils

/**
 * Representation of strict positive real number
 */
object Cost {
  implicit class Cost(val x: Double) extends Ordered[Cost] {
    assume(x > 0.0)

    /**
     * Returns the double representation of a cost
     */
    def toDouble: Double = x

    override def <=(cost : Cost) : Boolean = this.toDouble <= cost.toDouble

    override def compare(that: Cost): Int = this.toDouble.compare(that.toDouble)
  }
}
