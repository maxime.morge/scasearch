// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search

import fr.cristal.smac.sci.utils.Cost.Cost

import scala.language.implicitConversions

/**
 * Trait representing an optimization problem
 */
trait OptimizationProblem[State, Action]{


  /**
   * Returns the initial state
   */
  def initialState : State

  /**
   * Returns the legal actions in a state
   */
  def actions(state: State): List[Action]

  /**
   * Returns the outcome of an action in a state
   */
  def outcome(action: Action, state: State) : State

  /**
   * Returns the value of the objective function for a state
   */
  def objective(state: State) : Cost
}

