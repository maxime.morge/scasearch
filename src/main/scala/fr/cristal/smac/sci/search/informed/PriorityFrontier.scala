// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.informed

import fr.cristal.smac.sci.search._

import scala.collection.mutable
import scala.util.Try

/**
 * The priority queue pops the element of the queue with
 * the highest priority according to some ordering function.
 */
class PriorityFrontier[State, Action, Node <: HeuristicsNode[State, Action]](
       queue: mutable.PriorityQueue[Node],
       stateMap: mutable.Map[State, Node]
       ) extends Frontier[State, Action, Node] { self =>

  /**
   * Constructor method
   */
  def this(n: Node, costNodeOrdering: Ordering[Node]) =
    this(mutable.PriorityQueue(n)(costNodeOrdering), mutable.Map(n.state -> n))

  /**
   * Remove a leaf node from the frontier
   */
  def removeLeaf(): Option[(Node, Frontier[State, Action, Node])] =
    Try {
      val leaf = queue.dequeue
      stateMap -= leaf.state
      (leaf, self)
    }.toOption

  /**
   * All some nodes to the frontier
   */
  override def addAll(iterable: scala.Iterable[Node]): Frontier[State, Action, Node] = {
    iterable.foreach { node =>
      queue += node
      stateMap += (node.state -> node)
    }
    self
  }

  /**
   * Returns true if the frontier contain a node with the state
   */
  def contains(state: State): Boolean = stateMap.contains(state)

  /**
   * Replace a node with the same state
   */
  def replaceByState(node: Node): Frontier[State, Action, Node] = {
    if (contains(node.state)) {
      val updatedElems = node :: queue.toList.filterNot(_.state == node.state)
      queue.clear()
      queue.enqueue(updatedElems: _*)
      stateMap += (node.state -> node)
    }
    self
  }


  /**
   * Returns a node with a specific state
   */
  def getNode(state: State): Option[Node] = {
    if (contains(state)) {
      queue.find(_.state == state)
    } else {
      None
    }
  }

  /**
   * Add a node to the frontier
   */
  def add(node: Node): Frontier[State, Action, Node] = {
    val newNode = node
    queue.enqueue(newNode)
    stateMap += (node.state -> newNode)
    self
  }

}