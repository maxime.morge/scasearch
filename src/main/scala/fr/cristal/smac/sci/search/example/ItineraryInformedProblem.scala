// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.example

import fr.cristal.smac.sci.search.example.Itinerary._
import fr.cristal.smac.sci.search.informed.InformedProblem

/**
 * Class representing an itinerary problem
 * with specific knowledge
 * @param initialState is the initial state
 * @param goalState    is the final state
 */
class ItineraryInformedProblem(override val initialState: In, override val goalState: In) extends
  ItineraryProblem(initialState,goalState) with InformedProblem[In, Move] {
  /**
   * Returns a n estimation of the cost to get from the state to the goal
   */
  override def h(state: In): Double = straightLineDistance2Bucharest(state.city)
}