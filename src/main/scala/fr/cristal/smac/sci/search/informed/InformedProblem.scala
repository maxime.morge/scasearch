// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.informed

import fr.cristal.smac.sci.search.Problem
import fr.cristal.smac.sci.utils.Cost.Cost

import scala.language.implicitConversions

/**
 * Trait representing a problem with specific knowledge
 */
trait InformedProblem[State, Action] extends Problem[State, Action]{

  /**
   * Returns a n estimation of the cost to get from the state to the goal
   */
  def h(state: State) : Double

}

