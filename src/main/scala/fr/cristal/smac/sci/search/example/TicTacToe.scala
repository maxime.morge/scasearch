// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.example

import fr.cristal.smac.sci.search.ZeroSumGame
import fr.cristal.smac.sci.search.example.TicTacToe._

object TicTacToe {
  type Board = Array[Array[Char]]
  type Configuration = (String,Board)
  type Move = (Int,Int)
}

object TicTacToeGame extends ZeroSumGame[Configuration, Move] {

  val X = 'X'
  val O = 'O'
  private val nullChar: Char = 0

  /**
   * Returns a string representation of the state
   */
  def toString(state: Configuration): String = {
    val board = state._2
    var result = ""
    for(y <- 2.until(-1,-1); x <- 0 to 2) {
      result = result + (if(board(x)(y) == nullChar) "-" else board(x)(y))
      if(x == 2) result = result + "\n"
    }
    result
  }

  /**
   * Returns the initial state
   */
  override def initialState: (String, Board) = (ZeroSumGame.Max, Array.ofDim[Char](3, 3))

  /**
   * Returns the next player in a State
   */
  override def player(s: (String, Board)): String = s._1

  /**
   * Returns the legal actions in a state
   */
  override def actions(state: (String, Board)): List[(Int, Int)] = (for(x <- 0 to 2; y <- 0 to 2; if state._2(x)(y) == nullChar) yield (x,y)).toList

  /**
   * Returns the outcome of an action in a state
   */
  override def outcome(action: (Int, Int), state: (String, Board)): (String, Board) = {
    state match {
      case (ZeroSumGame.Max, board) =>
        if (board(action._1)(action._2) == nullChar) {
          val newBoard = cloneBoard(board)
          newBoard(action._1)(action._2) = X
          (ZeroSumGame.Min, newBoard)
        }
        else throw new IllegalStateException(s"Box at $action is already filled.")
      case (ZeroSumGame.Min, board) =>
        if (board(action._1)(action._2) == nullChar) {
          val newBoard = cloneBoard(board)
          newBoard(action._1)(action._2) = O
          (ZeroSumGame.Max, newBoard)
        }
        else throw new IllegalStateException(s"Box at $action is already filled.")
      case _ => throw new IllegalStateException(s" ${state._1} is not a valid player ")
    }
  }

  /**
   * Returns true if the state is the goal and false otherwise
   */
  override def isGoalState(state: (String, Board)): Boolean = winner(state._2) match {
    case Some(_) => true
    case None => actions(state).isEmpty
  }


  /**
   * Returns the utility of MAX,
   * a single value because the values are always opposite
   */
  override def utility(state: (String, Board)): Double = {
    winner(state._2) match {
      case Some(ZeroSumGame.Max) => 1.0
      case Some(ZeroSumGame.Min) => -1.0
      case None if actions(state).isEmpty => 0.5
      case _ => throw new IllegalStateException("Not a terminal state." + toString(state))
    }
  }

  /**
   * Returns the winner if game has terminated without draw,
   * None otherwise
   */
  private def winner(board: Board): Option[String] = {
    // The list of boxes in following order
    //((0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), (2,1), (2,2))
    (for( x <- 0 to 2; y <- 0 to 2) yield board(x)(y)).toList match {
      case X :: X :: X :: _ :: _ :: _ :: _ :: _ :: _ :: Nil => Some(ZeroSumGame.Max)
      case _ :: _ :: _ :: X :: X :: X :: _ :: _ :: _ :: Nil => Some(ZeroSumGame.Max)
      case _ :: _ :: _ :: _ :: _ :: _ :: X :: X :: X :: Nil => Some(ZeroSumGame.Max)
      case X :: _ :: _ :: X :: _ :: _ :: X :: _ :: _ :: Nil => Some(ZeroSumGame.Max)
      case _ :: X :: _ :: _ :: X :: _ :: _ :: X :: _ :: Nil => Some(ZeroSumGame.Max)
      case _ :: _ :: X :: _ :: _ :: X :: _ :: _ :: X :: Nil => Some(ZeroSumGame.Max)
      case X :: _ :: _ :: _ :: X :: _ :: _ :: _ :: X :: Nil => Some(ZeroSumGame.Max)
      case _ :: _ :: X :: _ :: X :: _ :: X :: _ :: _ :: Nil => Some(ZeroSumGame.Max)

      case O :: O :: O :: _ :: _ :: _ :: _ :: _ :: _ :: Nil => Some(ZeroSumGame.Min)
      case _ :: _ :: _ :: O :: O :: O :: _ :: _ :: _ :: Nil => Some(ZeroSumGame.Min)
      case _ :: _ :: _ :: _ :: _ :: _ :: O :: O :: O :: Nil => Some(ZeroSumGame.Min)
      case O :: _ :: _ :: O :: _ :: _ :: O :: _ :: _ :: Nil => Some(ZeroSumGame.Min)
      case _ :: O :: _ :: _ :: O :: _ :: _ :: O :: _ :: Nil => Some(ZeroSumGame.Min)
      case _ :: _ :: O :: _ :: _ :: O :: _ :: _ :: O :: Nil => Some(ZeroSumGame.Min)
      case O :: _ :: _ :: _ :: O :: _ :: _ :: _ :: O :: Nil => Some(ZeroSumGame.Min)
      case _ :: _ :: O :: _ :: O :: _ :: O :: _ :: _ :: Nil => Some(ZeroSumGame.Min)

      case _ => None
    }
  }

  /**
   * Returns a copy of the board
   */
  private def cloneBoard(board: Board): Board = {
    val copy = Array.ofDim[Char](3,3)
    for(x <- 0 to 2; y <- 0 to 2) {
      copy(x)(y) = board(x)(y)
    }
    copy
  }
}