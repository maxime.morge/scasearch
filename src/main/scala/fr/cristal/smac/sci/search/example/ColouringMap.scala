
// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.example

import fr.cristal.smac.sci.search.csp._

/**
 * We aim at colouring a map of Australia such that each region either
 * red, green, or blue
 * in such a way that no neighboring regions have the same color.
 */
object ColouringMap{

  val red : NominalValue = NominalValue("red")
  val green : NominalValue = NominalValue("green")
  val blue : NominalValue = NominalValue("blue")
  val colours = List(red, green, blue)
  val wa: Variable = Variable(name = "Western Australia", colours)
  val nt: Variable = Variable(name = "Northern Territory", colours)
  val q: Variable = Variable(name = "Queensland", colours)
  val nsw: Variable = Variable(name = "New South Wales", colours)
  val v: Variable = Variable(name = "Victoria", colours)
  val sa: Variable = Variable(name = "South Australia", colours)
  val t: Variable = Variable(name = "Tasmania", colours)
  val differentColours : Set[(Value, Value)]=
    Set((red, green), (red, blue), (green, red), (green, blue),
      (blue, red), (blue, green))
  val wa_nt: Constraint = Constraint(wa, nt, differentColours)
  val wa_sa: Constraint = Constraint(wa, sa, differentColours)
  val nt_sa: Constraint = Constraint(nt, sa, differentColours)
  val nt_q: Constraint = Constraint(nt ,q, differentColours)
  val sa_q: Constraint = Constraint(sa, q, differentColours)
  val sa_nsw: Constraint = Constraint(sa, nsw, differentColours)
  val sa_v: Constraint = Constraint(sa, v, differentColours)
  val q_nsw: Constraint = Constraint(q, nsw, differentColours)
  val nsw_v: Constraint = Constraint(nsw, v, differentColours)
  val csp: CSP = CSP(Set(wa, nt, q, nsw, v, sa, t),
    Set(wa_nt, wa_sa, nt_sa, nt_q, sa_q, sa_nsw, sa_v, q_nsw, nsw_v))
}
