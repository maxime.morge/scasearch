// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.local

import fr.cristal.smac.sci.search.OptimizationProblem
import fr.cristal.smac.sci.utils.MathUtils._

import scala.util.Random

/**
 * Simulated annealing combines hill climbing with a random walk that yields both efficiency and completeness.
 */
trait SimulatedAnnealingSearch[State, Action] {

  def initialTemperature : Double // e.g. 1, 10, 100
  def threshold : Double // e.g. 0.1, 0.01, 0.001
  def decayRate : Double // e.g. 0.9, 0.99, 0.999

  /**
   * Returns a random neighbor
   */
  def randomSuccessor(current: State, problem : OptimizationProblem[State, Action]): State = {
    val successors  = problem.actions(current).map(a => problem.outcome(a,current))
    successors(Random.nextInt(successors.length))
  }

  /**
   * Returns the acceptance probability that depends on the
   * @param currentEnergy energy of the current state
   * @param newEnergy energy of the new state
   * @param temperature a global time-varying parameter
   */
  def acceptanceProbability(currentEnergy : Double, newEnergy : Double, temperature : Double) : Double = {
    if (newEnergy ~< currentEnergy) return 1
    math.exp( -(newEnergy - currentEnergy) / temperature)
  }

  /**
   * Returns a local minima
   */
  def search(problem: OptimizationProblem[State, Action]): State = {
    var current = problem.initialState
    var temperature = initialTemperature
    while (temperature ~> threshold ){
      val neighbor = randomSuccessor(current, problem)
      val p = acceptanceProbability(problem.objective(current).toDouble, problem.objective(neighbor).toDouble, temperature)
      if ( p > Random.nextDouble() ) current = neighbor
      temperature *= decayRate
    }
    current
  }
}