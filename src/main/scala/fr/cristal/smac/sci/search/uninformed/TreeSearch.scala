// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.uninformed

import fr.cristal.smac.sci.search._

import scala.annotation.tailrec

/**
 * Generic tree search
 */
trait TreeSearch[State, Action] extends ProblemSearch[State, Action, SearchNode[State, Action]]
    with FrontierSearch[State, Action, SearchNode[State, Action]] {

  type Node = SearchNode[State, Action] // shortcut

  /**
   * Returns a sequence of actions to solve the problem
   */
  def search(problem: Problem[State, Action], noAction: Action): List[Action] = {

    // Setup the frontier
    val initialFrontier = newFrontier(problem.initialState, noAction)

    /**
     * Search by exploring the frontier
     */
    @tailrec def searchHelper(frontier: Frontier[State, Action, Node]): List[Action] = {
      frontier.removeLeaf() match { // Develop the next node
        case None  => // There is no more node at the frontier
          List.empty[Action]
        case Some((leaf, _)) if problem.isGoalState(leaf.state) => //  Either the goal is reached
          solution(leaf)
        case Some((leaf, updatedFrontier)) => // Otherwise
          val childNodes = for {
            action <- problem.actions(leaf.state)
          } yield develop(problem, leaf,   action) // develop all the children
          // update the frontier
          val frontierWithChildNodes = updatedFrontier.addAll(childNodes)
          // continue the search by exploring the frontier
          searchHelper(frontierWithChildNodes)
      }
    }

    // Search by exploring the initial frontier
    searchHelper(initialFrontier)
  }
}
