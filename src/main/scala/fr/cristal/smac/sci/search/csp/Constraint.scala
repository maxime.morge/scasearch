// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.csp

/**
  * Class representing a binary constraint over two variables
  * which valuates the cost of each couple of values
 *
  * @param varA the first variable
  * @param varB the second variable
  * @param values which are legal
  */
case class Constraint(varA: Variable, varB: Variable, values: Set[(Value,Value)]) {

  /**
    * String representation
    */
  override def toString: String = {
    s"Constraint(x${varA.name}, x${varB.name}= "+
    (for (xi <- varA.domain ; xj <- varB.domain if (values.contains(xi,xj)))
      yield s"($xi, $xj)").mkString("{",", ","}\n")
  }

  /**
    * Returns true if the constraint is sound, i.e
    * the cost of each couple of values is known and positive
    */
  def sound() : Boolean =
    values.forall(  v => varA.domain.contains(v._1) && varB.domain.contains(v._2)) &&
      values.nonEmpty

  /**
    * Returns true if the constraint is over a specific variable
    */
  def isOver(variable: Variable) : Boolean = variable == varA || variable == varB

  /**
    * Returns true if the variable is satisfied when
    * the variable varA is assigned to the value val1
    * and the variable varB is assigned to the value val2
    */
  def isSatisfied(val1: Value, val2: Value): Boolean =
    values.contains((val1, val2))

  /**
    * Returns true is the constraint is satisfied in a assignment,
    * eventually true if one of the variable is not instantiated
    */
  def isSatisfied(assignment: Assignment): Boolean = {
    if (assignment.getValue(varA).isEmpty || assignment.getValue(varB).isEmpty)
      //throw new RuntimeException(s"The satisfaction of the constraint over $varA and $varB is not defined")
      return true
    isSatisfied(assignment.getValue(varA).get, assignment.getValue(varB).get)
  }
}

