// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.informed

import fr.cristal.smac.sci.search._


/**
  * Greedy best-first search7 tries to expand the node that is closest to the goal,
 * on the grounds that this is likely to lead to a solution quickly.
 * Thus, it evaluates nodes by using just the heuristic function: f(n) = h(n).
 */
trait Greedy[State, Action]  extends FirstSearch[State, Action]{

  override def newFrontier(problem: InformedProblem[State, Action], state: State, noAction: Action): PriorityFrontier[State, Action, Node] = {
    val costNodeOrdering: Ordering[Node] = (n1: Node, n2: Node) => Ordering[Double].reverse.compare(n1.fValue, n2.fValue)
    new PriorityFrontier[State, Action, Node](
      HeuristicsNode(
        state,
        noAction,
        None,
        0.0,
        problem.h(state).toDouble,
        0.0),
      costNodeOrdering)
  }

  override def develop(problem: InformedProblem[State, Action], parent: Node, action: Action): Node = {
    val childState = problem.outcome(action, parent.state)
    val g : Double = parent.gValue +  problem.cost(parent.state, action).toDouble
    val h : Double = problem.h(childState).toDouble
    HeuristicsNode(
      state = childState,
      parent = Some(parent),
      action = action,
      gValue = g,
      hValue = h,
      fValue = h
    )
  }
}