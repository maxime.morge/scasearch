// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.csp
import domain.Domain

import scala.collection.mutable

/**
  * Class representing a Constraint Satisfaction Problem
  * @param variables for which values must be given
  * @param constraints which must be satisfied
  */
case class CSP(variables: Set[Variable], constraints: Set[Constraint]) {

  val debug = false

  /**
    * String representation
    */
  override def toString: String = s"Variables:\n"+
    variables.map(v => v.description).mkString("\t", "\n\t", "\n")+
  "Constraints:\n"+
    constraints.mkString("\t", "\n\t", "\n")

  /**
    * Returns the size of the DCOP
    */
  def size: Int = variables.size

  /**
    * Returns true if the DCOP is sound, i.e.
    * 1 -there is at least one variable,
    * 2- each variable has a unique id
    * all the constraints are sound
    */
  def isSound : Boolean = variables.nonEmpty &&
    variables.map(_.name).size == variables.size &&
    constraints.forall( c => c.sound())

  /**
    * Returns the other variables linked to the variable
    */
  def linkedVariablesOf(variable : Variable) :  Set[Variable] = {
    var variables =  Set[Variable]()
    constraints.foreach{ constraint =>
      if (constraint.varA == variable)  variables = variables + constraint.varB
      if (constraint.varB == variable)  variables = variables + constraint.varA
    }
    if (debug) println(s"DCOP: linked variables of $variable=$variables")
    variables
  }

  /**
    * Returns the constraints over a specific variable
    */
  def constraintsOf(variable: Variable) : Set[Constraint] =
    constraints.filter(c => c.varA == variable || c.varB == variable)

  /**
   * Returns the constraint over two specific variables
   * we assume there is at most a single constraint over a couple
   */
  def constraintOf(variable1: Variable, variable2: Variable) : Option[Constraint] =
    constraints.find(c => (c.varA == variable1 && c.varB == variable2 ) || (c.varA == variable2 && c.varB == variable1 ))

  /**
    * Returns true if a assignment is sound,
    * i.e. each value is in the corresponding domain
    */
  def isSound(context: Assignment) : Boolean = {
    variables.foreach{ variable =>
      if (! variable.domain.contains(context.valuation(variable))) return  false
    }
    true
  }

  /**
    * Returns true if the assignment is an assignment, i.e. a full assignment
    */
  def isFull(context: Assignment): Boolean = context.valuation.size == variables.size

  /**
    * Returns true if an assignment is a solution,
   * i.e. a full assignment which satisfies all the constraints
    */
  def isSolution(context: Assignment) : Boolean = {
    if (! isFull(context))
      throw new RuntimeException(s"Error: the assignment $context is not a (full) assignment")
    constraints.forall(constraint  =>
      constraint.isSatisfied(context.valuation(constraint.varA), context.valuation(constraint.varB)))
  }

  /**
   * Returns a queue with all the arcs
   */
  def allArcs : mutable.Queue[(Variable, Variable)] =
    constraints.foldLeft(mutable.Queue[(Variable, Variable)]())((acc, c) => acc.enqueue((c.varA, c.varB)).enqueue((c.varB, c.varA)))

  /**
   * Change the domain of a variable
    */
  def changeDomain(variable: Variable, domain : Domain) : CSP =
     CSP(variables - variable + Variable(variable.name, domain), constraints)
}

