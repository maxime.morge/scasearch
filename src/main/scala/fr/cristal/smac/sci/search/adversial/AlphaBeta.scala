// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.adversial

import fr.cristal.smac.sci.search.ZeroSumGame
import fr.cristal.smac.sci.utils.RandomUtils.random

import scala.language.postfixOps

/**
 * The alpha-beta pruning returns the same move as minimax would,
 * but prunes away branches that cannot possibly influence the final decision
 */
class AlphaBeta[State,Action](val game : ZeroSumGame[State,Action]) {

    /**
     * Computes recursively the minimax values of each successor state
     */
    def minMaxDecision(state: State) : Action = {
      // The pairs (legalAction, valueSuccessor)
      val pairs : List[(Action,Double)]= game.actions(state).map(action =>
        (action,minValue(game.outcome(action,state),Double.MinValue,Double.MaxValue))
      )
      // Sorts the pairs in descending order of MinValue
      val sortedPairs = pairs.sortWith(_._2 > _._2)
      // Takes the pairs with highest minValue
      val bestPairs = sortedPairs.takeWhile(sortedPairs.head._2 == _._2)
      // Randomly chooses one
      random(bestPairs)._1
    }

    /**
     * Goes through the game tree to return the backed-up value of a state
     * within the bounds on the backed.-upvalues alpha and beta
     */
    private def maxValue(state: State, alpha: Double, beta: Double): Double = {
      if (game.isGoalState(state)) return game.utility(state)
      val successors = game.actions(state).map(action => game.outcome(action,state))
      successors.foldLeft(Double.MinValue){
        (maxValue,successor) =>
          val currentValue = minValue(successor, alpha, beta)
          if (beta <= currentValue) maxValue
          else minValue(successor, Math.max(alpha,currentValue), beta)
      }
    }

    /**
     * Goes through the game tree to return the backed-up value of a state
     * within the bounds on the backed.-upvalues alpha and beta
     */
    private def minValue(state: State, alpha: Double, beta: Double): Double = {
      if (game.isGoalState(state)) return game.utility(state)
      val successors = game.actions(state).map(action => game.outcome(action,state))
      successors.foldLeft(Double.MaxValue){
        (minValue, successor) =>
        val currentValue = maxValue(successor, alpha, beta)
        if (currentValue <= alpha) minValue
        else maxValue(successor, currentValue, Math.min(beta,currentValue))
      }
    }
}
