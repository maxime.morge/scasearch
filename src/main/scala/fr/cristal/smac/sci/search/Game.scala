// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search

/**
 * Abstract representation of a Game,
 */
abstract class Game[Player,State,Action] {
  /**
   * Returns the initial state
   */
  def initialState : State

  /**
   * Returns true if the state is the goal and false otherwise
   */
  def isGoalState(state: State) : Boolean

  /**
   * Returns the next player in a State
   */
  def player(s: State): Player

  /**
   * Returns the legal actions in a state
   */
  def actions(state: State): List[Action]

  /**
   * Returns the outcome of an action in a state
   */
  def outcome(action: Action, state: State) : State

  /**
   * Returns the utility of a player in a state
   */
  def utility(state: State, player: Player): Double
}

/**
 * Abstract representation for zero-sum 2-player games
 * where MIN and MAX play against each other
 */
abstract class ZeroSumGame[State,Action] extends Game[String,State,Action] {
  /**
   * Returns the utility of a player in a state
   */
  override def utility(state: State, player: String): Double = utility(state)

  /**
   * Returns the utility of MAX,
   * a single value because the values are always opposite
   */
  def utility(state: State): Double

}

/**
 * Companion object
 */
object ZeroSumGame {
  val Min = "MIN"
  val Max = "MAX"
}
