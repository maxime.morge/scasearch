// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.local

import fr.cristal.smac.sci.search.OptimizationProblem

import scala.annotation.tailrec

/**
 * HillClimbing is an iterative greedy local search
 */
trait HillClimbing[State, Action] {

  /**
   * Returns the best state which is a neighbor with the lowest objective function value
   */
  def bestSuccessor(current: State, problem : OptimizationProblem[State, Action]): State = {
    val successors = problem.actions(current).map(a => problem.outcome(a,current))
    if (successors.isEmpty) current
    else successors.minBy(problem.objective)
  }

  /**
   * Returns a local minima
   */
  def search(problem: OptimizationProblem[State, Action]): State = {
    /**
     * Returns the closest local minima from a current state
     */
    @tailrec def search(current: State): State = {
      val neighbor = bestSuccessor(current, problem)
      if (problem.objective(neighbor) >= problem.objective(current)) {
        current
      } else {
        search(neighbor)
      }
    }
    // Search from the initial state
    search(problem.initialState)
  }

}