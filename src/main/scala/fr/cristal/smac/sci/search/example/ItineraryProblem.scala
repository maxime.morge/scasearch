// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.example

import fr.cristal.smac.sci.search.Problem
import fr.cristal.smac.sci.search.example.Itinerary._
import fr.cristal.smac.sci.utils.Cost.Cost

/**
 * Class representing an itinerary problem
 *
 * @param initialState is the initial state
 * @param goalState    is the final state
 */
class ItineraryProblem(val initialState: In, val goalState: In)
  extends Problem[In, Move] {

  /**
   * Returns true if the state is the goal and false otherwise
   */
  override def isGoalState(state: In): Boolean = state == goalState

  /**
   * Returns the legal actions in a state
   */
  override def actions(state: In): List[Move] =
    roads(state.city).map(road => Goto(road.destination))

  /**
   * Returns the outcome of an action in a state
   */
  override def outcome(action: Move, state: In): In = action match {
    case Goto(city) => In(city)
    case Stay => state
  }

  /**
   * Returns the cost of an action in a state
   */
  override def cost(state: In, action: Move): Cost = action match {
    case Goto(destination) =>
      val potentialCost = roads(state.city).collectFirst {
        case Road(c1, c2, cost) if c1 == state.city && c2 == destination => cost
      }
      potentialCost.getOrElse(Double.MaxValue)
    case Stay => 0.0
  }
}