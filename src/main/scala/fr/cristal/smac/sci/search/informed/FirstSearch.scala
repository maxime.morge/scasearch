// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.informed

import fr.cristal.smac.sci.search._

import scala.annotation.tailrec

/**
 * Best-first search is an instance of the general TREE-SEARCH or GRAPH-SEARCH algorithm
 * in which a node is selected for expansion based on an evaluation function,
 */
trait FirstSearch[State, Action]
  extends ProblemSearch[State, Action, HeuristicsNode[State, Action]]
    with FrontierSearch[State, Action, HeuristicsNode[State, Action]] {

  type Node = HeuristicsNode[State, Action]

  /**
    * Returns a sequence of actions to solve the problem
   */
  def search(problem: InformedProblem[State, Action], noAction: Action): List[Action] = {
    // Setup the frontier
    val initialFrontier = newFrontier(problem, problem.initialState, noAction)

    /**
     * Search by exploring the frontier
     */
    @tailrec def searchHelper(
      frontier: Frontier[State, Action, Node],
      exploredSet: Set[State] = Set.empty[State]
      ): List[Action] = {

      frontier.removeLeaf() match { // Develop the next node
        case None => // There is no more node at the frontier
          List.empty[Action]
        case Some((leaf, _)) if problem.isGoalState(leaf.state) => //  Either the goal is reached
          solution(leaf)
        case Some((leaf, updatedFrontier)) => // Otherwise
          val updatedExploredSet = exploredSet + leaf.state
          val childNodes = for {
            action <- problem.actions(leaf.state)
          } yield develop(problem, leaf, action) // develop all the children

          // update the frontier
          val frontierWithChildNodes = childNodes.foldLeft(updatedFrontier) { (accFrontier, childNode) =>
            // which are unexplored and already in the frontier
            if (!(updatedExploredSet.contains(childNode.state) || accFrontier.contains(childNode.state))) {
              accFrontier.add(childNode)
            } else if (accFrontier
              .getNode(childNode.state)
              .exists(existingNode => existingNode.fValue > childNode.fValue)) {
              accFrontier.replaceByState(childNode) // replace with better node
            } else {
              accFrontier
            }
          }
          // continue the search by exploring the frontier
          searchHelper(frontierWithChildNodes, updatedExploredSet)
      }
    }
    // Search by exploring the initial frontier
    searchHelper(initialFrontier)
  }

  def newFrontier(problem: InformedProblem[State, Action], state: State, noAction: Action): PriorityFrontier[State, Action, Node] = ???

  def develop(problem: InformedProblem[State, Action], parent: Node, action: Action): Node = ???
}