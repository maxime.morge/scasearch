// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.csp

/**
  * Class representing a full or partial assignment
  * @param valuation for some variables, eventually none
  */
case class Assignment(valuation: Map[Variable, Value] = Map()) {

  /**
    * String representation of the assignment
    */
  override def toString: String = valuation.mkString("[",", ","]")

  /**
    * Returns the number of instantiated variables
    */
  def size : Int = valuation.size

  /**
   * Returns the variables assigned
   */
  def assignedVariables: Set[Variable] = valuation.keySet

  /**
    * Returns the value for a variable
    */
  def getValue(variable: Variable) : Option[Value] = {
    if (valuation.contains(variable)) return Some(valuation(variable))
    None
  }

  /**
    * Returns a assignment by fixing the value of a variable
    */
  def fix(variable : Variable, value: Value) : Assignment = {
    if (! variable.domain.contains(value))
      throw new RuntimeException(s"$variable=$value is forbidden since $value is not in ${variable.domain}")
    new Assignment(valuation + (variable -> value))
  }

  /**
    * Fix the values of variables in the assignment
    */
  def fix(values :  Map[Variable, Value]) : Assignment ={
    var currentContext = this
    values.foreach{ case (variable, value) =>
      currentContext = currentContext.fix(variable,value)
    }
    currentContext
  }

  /**
    * Remove a variable in the assignment
    */
  def remove(variable: Variable) : Assignment= new Assignment(valuation - variable)

  /**
    * Returns true if the assignment is compatible with otherAssignment, i.e.
    * they do not disagree on any variable assignment
    */
  def isCompatible(otherAssignment : Assignment) : Boolean =
    this.valuation.keys.toSet.intersect(otherAssignment.valuation.keys.toSet).forall(variable =>
      this.valuation(variable) == otherAssignment.valuation(variable)
    )

  /**
    * Returns true if the constraints are satisfied by the assignment
    */
  def areSatisfied(constraints: Set[Constraint]): Boolean =
    constraints.forall(constraint =>
      constraint.isSatisfied(this))
}
