// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.uninformed

import fr.cristal.smac.sci.search._

import scala.annotation.tailrec

/**
 * Search when the space state is not a tree
 */
trait GraphSearch[State, Action] extends ProblemSearch[State, Action, StateNode[State, Action]]
  with FrontierSearch[State, Action, StateNode[State, Action]] {

  type Node = StateNode[State, Action] // shortcut

  /*
   * Returns a sequence of actions to solve the problem
   */
  def search(problem: Problem[State, Action], noAction: Action): List[Action] = {
      // Setup the initial frontier
      val initialFrontier = newFrontier(problem.initialState, noAction)

      /*
      * Search by exploring the frontier
      */
      @tailrec def searchHelper(
                                 frontier: Frontier[State, Action, Node],
                                 exploredSet: Set[State] = Set.empty[State]
                               ): List[Action] = {
        frontier.removeLeaf() match { // Develop the next node
          // There is no more node at the frontier
          case None  =>
            List.empty[Action]
          //  Either the goal is reached
          case Some((leaf, _)) if problem.isGoalState(leaf.state) =>
            solution(leaf)
          // Otherwise
          case Some((leaf, updatedFrontier)) =>
            val updatedExploredSet = exploredSet + leaf.state
            // develop the children
            val childNodes = for {
              action <- problem.actions(leaf.state)
              childNode = develop(problem, leaf, action)
              // which are unexplored and not yet in the frontier
              if !(updatedExploredSet.contains(childNode.state)
                || updatedFrontier.contains(childNode.state))
            } yield childNode
            // update the frontier
            val frontierWithChildNodes = updatedFrontier.addAll(childNodes)
            // continue the search by exploring the frontier
            searchHelper(frontierWithChildNodes, updatedExploredSet)
        }
      }
      // Search by exploring the initial frontier
      searchHelper(initialFrontier)
    }

  /**
   * Returns the next node by performing the action in the state of the parent for the problem
   */
  def develop(problem: Problem[State, Action], parent: Node, action: Action): Node = {
    val childState = problem.outcome(action, parent.state)
    StateNode(childState, action, Some(parent))
  }
}
