// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.uninformed

import fr.cristal.smac.sci.search._

/**
 * Depth-first search (DFS) algorithm for searching graph.
 *  The search proceeds immediately to the deepest level
 *  of the search tree, where the nodes have no successors.
 */
  trait DepthFirstSearch[State, Action] extends GraphSearch[State, Action] {
    self =>
    def newFrontier(state: State, noAction: Action) =
      new LIFOFrontier(StateNode(state, noAction, None))
  }

/**
 * Depth-first search is implemented by calling search with
 * an empty frontier that is a last-in-first-out (LIFO) queue,
 * assuring that the nodes that are visited first will be expanded first.
 */
class LIFOFrontier[State, Action, Node <: SearchNode[State, Action]](stack: List[Node], stateSet: Set[State])
  extends Frontier[State, Action, Node] { self =>

  /**
   * Constructor method
   */
  def this(n: Node) = this(List(n), Set(n.state))

  /**
   * Removes a node from the frontier and returns
   * the node and the frontier without the node
   */
  def removeLeaf(): Option[(Node, Frontier[State, Action, Node])] = stack match {
    case leaf :: updatedStack =>
      Some(leaf, new LIFOFrontier[State, Action, Node](updatedStack, stateSet - leaf.state))
  }

  /**
   * Adds a set of nodes to the frontier
   */
  def addAll(iterable: scala.Iterable[Node]): Frontier[State, Action, Node] =
    new LIFOFrontier( stack ++ iterable, stateSet ++ iterable.map(_.state))

  /**
   * Returns true if the state is in the frontier
   */
  def contains(state: State): Boolean = stateSet.contains(state)

  /**
   * Returns the frontier by replacing an existing node with the
   * current state if any node has the same state in the frontier
   */
  def replaceByState(node: Node): Frontier[State, Action, Node] = {
    if (contains(node.state)) {
      new LIFOFrontier(node :: stack.filterNot(_.state == node.state), stateSet)
    } else {
      self
    }
  }

  /**
   * Returns the node with a state if any
   * none otherwise
   */
  def getNode(state: State): Option[Node] = {
    if (contains(state)) {
      stack.find(_.state == state)
    } else {
      None
    }
  }

  /**
   * Adds a node to the frontier
   */
  def add(node: Node): Frontier[State, Action, Node] =
    new LIFOFrontier[State, Action, Node](node :: stack, stateSet + node.state)
}


