// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search

import fr.cristal.smac.sci.utils.Cost.Cost

import scala.language.implicitConversions

/**
 * Trait representing a problem
 */
trait Problem[State, Action]{

  /**
   * Returns the initial state
   */
  def initialState : State

  /**
   * Returns true if the state is the goal and false otherwise
   */
  def isGoalState(state: State) : Boolean

  /**
   * Returns the legal actions in a state
   */
  def actions(state: State): List[Action]

  /**
   * Returns the outcome of an action in a state
   */
  def outcome(action: Action, state: State) : State

  /**
   * Returns the cost of an action in a state
   */
  def cost(state: State, action: Action) : Cost
}

