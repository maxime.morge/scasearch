// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search

import fr.cristal.smac.sci.utils.Cost.Cost

import scala.annotation.tailrec

/**
 * Node of a tree search
 */
sealed trait SearchNode[State, Action]{
  def state : State
  def action : Action
  def parent : Option[SearchNode[State, Action]]
}

/**
 * Node of a graph search
 */
final case class StateNode[State, Action](
  state : State,
  action : Action,
  parent : Option[StateNode[State, Action]]
) extends SearchNode[State, Action]

/**
 * Node of an informed search
 */
final case class HeuristicsNode[State, Action](
  state: State,
  action : Action,
  parent : Option[HeuristicsNode[State, Action]],
  gValue: Double,
  hValue: Double,
  fValue: Double
  ) extends SearchNode[State, Action]

/**
 * Frontier of a tree search
 */
trait Frontier[State, Action, Node <: SearchNode[State, Action]] {
  def replaceByState(childNode: Node): Frontier[State, Action, Node]
  def getNode(state: State): Option[Node]
  def removeLeaf(): Option[(Node, Frontier[State, Action, Node])]
  def add(node: Node): Frontier[State, Action, Node]
  def addAll(iterable: Iterable[Node]): Frontier[State, Action, Node]
  def contains(state: State): Boolean
}

/**
 * Basic problem search
 */
trait ProblemSearch[State, Action, Node <: SearchNode[State, Action]] {
  /**
   * Returns the next node after performing the action in the state of the parent
   */
  def develop(problem: Problem[State, Action], parent: Node, action: Action): Node

  /**
   * Returns the sequence of actions to reach a node
   */
  def solution(node: Node): List[Action] = {
    /**
     * Reverse the sequence of actions
     */
    @tailrec def solutionHelper(n: Node, actions: List[Action]): List[Action] = {
      n.parent match {
        case None  =>
          actions
        case Some(parent: Node) =>
          solutionHelper(parent, n.action :: actions)
      }
    }
    solutionHelper(node, Nil)
  }
}

/**
 * Basic frontier search
 */
trait FrontierSearch[State, Action, Node <: SearchNode[State, Action]] {
  def newFrontier(state: State, noAction: Action): Frontier[State, Action, Node]
}