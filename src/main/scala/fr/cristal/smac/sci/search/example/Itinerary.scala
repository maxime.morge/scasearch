// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.example

import fr.cristal.smac.sci.search.Problem
import fr.cristal.smac.sci.search.informed.InformedProblem
import fr.cristal.smac.sci.utils.Cost.Cost

/**
 * Find an itinerary origin Arad destination Bucharest
 */
object Itinerary {

  /**
   * The cities
   */
  sealed class City(name: String)

  case object Arad extends City("Arad")

  case object Zerind extends City("Zerind")

  case object Oradea extends City("Oradea")

  case object Sibiu extends City("Sibiu")

  case object Timisoara extends City("Timisoara")

  case object Lugoj extends City("Lugoj")

  case object Mehadia extends City("Mehadia")

  case object Drobeta extends City("Drobeta")

  case object RimnicuVilcea extends City("Rimnicu Vilcea")

  case object Craiova extends City("Craiova")

  case object Fagaras extends City("Fagaras")

  case object Pitesti extends City("Pitesti")

  case object Bucharest extends City("Bucharest")

  case object Giurgiu extends City("Giurgiu")

  case object Urziceni extends City("Urziceni")

  case object Neamt extends City("Neamt")

  case object Iasi extends City("Iasi")

  case object Vaslui extends City("Vaslui")

  case object Hirsova extends City("Hirsova")

  case object Eforie extends City("Eforie")

  /**
   * The roads
   */
  case class Road(origin: City, destination: City, cost: Cost)

  val roads: Map[City, List[Road]] = List(
    Road(Arad, Zerind, 75),
    Road(Arad, Timisoara, 118),
    Road(Arad, Sibiu, 140), //Arad Edges
    Road(Zerind, Arad, 75),
    Road(Zerind, Oradea, 71), //Zerind Edges
    Road(Oradea, Zerind, 71),
    Road(Oradea, Sibiu, 151), //Oradea Edges
    Road(Sibiu, Arad, 140),
    Road(Sibiu, Oradea, 151),
    Road(Sibiu, Fagaras, 99),
    Road(Sibiu, RimnicuVilcea, 80), //Sibiu Edges
    Road(Timisoara, Arad, 118),
    Road(Timisoara, Lugoj, 111), //Timisoara Edges
    Road(Lugoj, Timisoara, 111),
    Road(Lugoj, Mehadia, 70), //Lugoj Edges
    Road(Mehadia, Lugoj, 70),
    Road(Mehadia, Drobeta, 75), //Mehadia Edges
    Road(Drobeta, Mehadia, 75),
    Road(Drobeta, Craiova, 120), //Drobeta Edges
    Road(RimnicuVilcea, Sibiu, 80),
    Road(RimnicuVilcea, Craiova, 146),
    Road(RimnicuVilcea, Pitesti, 97), //Rimnicu Vilcea Edges
    Road(Craiova, Drobeta, 120),
    Road(Craiova, RimnicuVilcea, 146),
    Road(Craiova, Pitesti, 138), //Craiova Edges
    Road(Fagaras, Sibiu, 99),
    Road(Fagaras, Bucharest, 211), //Fagaras Edges
    Road(Pitesti, Craiova, 138),
    Road(Pitesti, RimnicuVilcea, 97),
    Road(Pitesti, Bucharest, 101), //Pitesti Edges
    Road(Bucharest, Pitesti, 101),
    Road(Bucharest, Fagaras, 211),
    Road(Bucharest, Urziceni, 85),
    Road(Bucharest, Giurgiu, 90), //Bucharest Edges
    Road(Giurgiu, Bucharest, 90), //Giurgiu Edges
    Road(Urziceni, Bucharest, 85),
    Road(Urziceni, Vaslui, 142),
    Road(Urziceni, Hirsova, 98), //Urziceni Edges
    Road(Neamt, Iasi, 87), //Neamt Edges
    Road(Iasi, Neamt, 87),
    Road(Iasi, Vaslui, 92), //Iasi Edges
    Road(Vaslui, Iasi, 92),
    Road(Vaslui, Urziceni, 142), //Vaslui Edges
    Road(Hirsova, Urziceni, 98),
    Road(Hirsova, Eforie, 86), //Hirsova Edges
    Road(Eforie, Hirsova, 86) //Eforie Edges
  ).foldLeft(Map.empty[City, List[Road]]) { (acc, road) =>
    val from = road.origin
    val edgeList = acc.getOrElse(from, List.empty[Road])
    acc.updated(from, road :: edgeList)
  }

  /**
   * The straight-line distances to Bucharest
   */
  val straightLineDistance2Bucharest: Map[City, Double] = Map(
    Arad -> 366.0,
    Bucharest -> 0.0,
    Craiova -> 160.0,
    Drobeta -> 242.0,
    Eforie -> 161.0,
    Fagaras -> 176.0,
    Giurgiu -> 77.0,
    Hirsova -> 151.0,
    Iasi -> 226.0,
    Lugoj -> 244.0,
    Mehadia -> 241.0,
    Neamt -> 234.0,
    Oradea -> 380.0,
    Pitesti -> 100.0,
    RimnicuVilcea ->193.0,
    Sibiu -> 253.0,
    Timisoara -> 329.0,
    Urziceni -> 80.0,
    Vaslui -> 199.0,
    Zerind ->374.0)

  /**
   * The states
   */
  sealed case class In(city: City)

  /**
   * The actions
   */
  sealed trait Move

  final case class Goto(city: City) extends Move

  case object Stay extends Move

}
