// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.uninformed

import fr.cristal.smac.sci.search._

import scala.collection.immutable.Queue

/**
 * Breadth-first search (BFS) algorithm for searching graph.
 * It starts at the initial node of a graph and explores
 * all of the neighbor nodes at the present depth prior
 * to moving on to the nodes at the next depth level
 */
trait BreadthFirstSearch[State, Action] extends GraphSearch[State, Action] {
  self =>
  def newFrontier(state: State, noAction: Action) =
    new FIFOFrontier(StateNode(state, noAction, None))
}

/**
 * Breadth-first search is implemented by calling search with
 * an empty frontier that is a first-in-first-out (FIFO) queue,
 * assuring that the nodes that are visited first will be expanded first.
 */
class FIFOFrontier[State, Action, Node <: SearchNode[State, Action]](queue: Queue[Node], stateSet: Set[State])
  extends Frontier[State, Action, Node] { self =>

  /**
   * Constructor method
   */
  def this(n: Node) = this(Queue(n), Set(n.state))

  /**
   * Removes a node from the frontier and returns
   * the node and the frontier without the node
   */
  def removeLeaf(): Option[(Node, Frontier[State, Action, Node])] = queue.dequeueOption.map {
    case (leaf, updatedQueue) =>
      (leaf, new FIFOFrontier[State, Action, Node](updatedQueue, stateSet - leaf.state))
  }

  /**
   * Adds a set of nodes to the frontier
   */
  def addAll(iterable: scala.Iterable[Node]): Frontier[State, Action, Node] =
    new FIFOFrontier(queue.enqueueAll(iterable), stateSet ++ iterable.map(_.state))

  /**
   * Returns true if the state is in the frontier
   */
  def contains(state: State): Boolean = stateSet.contains(state)

  /**
   * Returns the frontier by replacing an existing node with the
   * current state if any node has the same state in the frontier
   */
  def replaceByState(node: Node): Frontier[State, Action, Node] = {
    if (contains(node.state)) {
      new FIFOFrontier(queue.filterNot(_.state == node.state).enqueue(node), stateSet)
    } else {
      self
    }
  }

  /**
   * Returns the node with a state if any
   * none otherwise
   */
  def getNode(state: State): Option[Node] = {
    if (contains(state)) {
      queue.find(_.state == state)
    } else {
      None
    }
  }

  /**
   * Adds a node to the frontier
   */
  def add(node: Node): Frontier[State, Action, Node] =
    new FIFOFrontier[State, Action, Node](queue.enqueue(node), stateSet + node.state)
}


