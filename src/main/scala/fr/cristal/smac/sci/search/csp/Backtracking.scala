// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.csp

import fr.cristal.smac.sci.search.csp.domain.Domain

import scala.collection.mutable

/**
 * Depth-first search for a CSP that chooses values for one variable at a time and
 * backtracks when a variable has no legal values left to assign.
 */
class Backtracking{
  /**
   * A simple backtracking algorithm for constraint satisfaction problems
   */
  def search(csp: CSP) : Option[Assignment] = recursiveSearch(new Assignment(), csp)

  /**
   * Extends the current assignment to generate a successor
   */
  def recursiveSearch(assignment: Assignment, csp: CSP): Option[Assignment] = {
    if (csp.isFull(assignment)) return Some(assignment)
    val variable : Variable= selectUnassignedVariable(assignment, csp)
    for (value <-  orderedDomainValues(variable, assignment, csp) ){
      var result : Option[Assignment] = None
      val newAssignment = assignment.fix(variable, value)
      if (newAssignment.areSatisfied(csp.constraints)){
        val newCSP = ac3(csp)
        result = recursiveSearch(newAssignment, newCSP)
        if (result.nonEmpty) return result
      }
    }
    None
  }

  /**
   * Select an unassigned variable in an assignment for a CSP
   * with the minimum remaining values (MRV) heuristic
   * and the degree heuristic as a tie-breaker
   */
  def selectUnassignedVariable(assignment: Assignment, csp: CSP): Variable = {
    //(csp.variables -- assignment.assignedVariables).head is a static arbitrary order
    highestDegree(minimumRemainingValues(assignment, csp), csp).head
  }

  /**
   * Returns an ordered list of values for a variable after
   * an assignment for a csp
   */
  def orderedDomainValues(variable: Variable, assignment: Assignment, csp : CSP) : List[Value] = {
    //variable.domain is a static arbitrary order
    leastConstrainingValue(variable, assignment, csp)
  }

  /**
   * Return the most constrained unassigned variable
   */
  def minimumRemainingValues(assignment: Assignment, csp: CSP): Set[Variable] = {
    var selectedVariables = Set[Variable]()
    val unassignedVariables = csp.variables -- assignment.assignedVariables
    var mrv = Int.MaxValue
    for(variable <- unassignedVariables){
      var nbLegalValues = 0
      for(value <- variable.domain) {
        if (assignment.fix(variable,value).areSatisfied(csp.constraints)) nbLegalValues +=1
      }
      if (nbLegalValues < mrv) {
        mrv = nbLegalValues
        selectedVariables = Set(variable)
      } else if(nbLegalValues == mrv){
        selectedVariables += variable
      }
    }
    selectedVariables
  }

  /**
   * Returns the unassigned variables with the highest degrees
   */
  def highestDegree(variables: Set[Variable], csp: CSP) : Set[Variable] = {
    var selectedVariables = Set[Variable]()
    var highestDegree = 0
    for (variable <- variables){
      val degree = csp.constraints.count(_.isOver(variable))
      if (degree > highestDegree) {
        highestDegree = degree
        selectedVariables = Set(variable)
      } else if (degree == highestDegree){
        selectedVariables += variable
      }
    }
    selectedVariables
  }

  /**
   *  Returns the value that rules out the fewest choices for the neighboring variables in the constraint graph.
   */
  def leastConstrainingValue(variable: Variable, assignment: Assignment, csp: CSP): List[Value] = {
    val unassignedNeighbours: Set[Variable] = csp.linkedVariablesOf(variable) -- assignment.assignedVariables
    (for (value <- variable.domain)
      yield (value, unassignedNeighbours.foldLeft(0){ (acc, n) =>
          acc + n.domain.foldLeft(0){ (acc2, nv) =>
            acc2 + assignment.fix(n,nv).areSatisfied(csp.constraints).compare(false)
          }
      })).sortWith( (a,b) => a._2 > b._2).map(_._1)
  }

  /**
   * Returns a new CSP where arc consistency is checked if any
   */
  def ac3(csp: CSP) : CSP = {
    val queue: mutable.Queue[(Variable, Variable)] = csp.allArcs
    var newCSP : CSP = csp.copy()
    while (queue.nonEmpty) {
      val (xi, xj) = queue.dequeue()
      val newDomainXi = removeInconsistentValue(xi, xj, newCSP)
      if (newDomainXi.isDefined) {
        newCSP = newCSP.changeDomain(xi, newDomainXi.get)
        for (xk <- csp.linkedVariablesOf(xi) - xj) {
          queue.enqueue((xk, xi))
        }
      }
    }
    newCSP
  }

  /**
   * Returns the new domain of xi if changed or None
   */
  def removeInconsistentValue(xi: Variable, xj: Variable, csp : CSP) : Option[Domain] = {
    var domain = xi.domain
    for(vi <- xi.domain){
      if (xj.domain.forall(vj => !csp.constraintOf(xi,xj).get.isSatisfied(Assignment(Map(xi->vi, xj->vj))))) {
        domain = domain.filter(_ == vi)
      }
    }
    if (domain.size == xi.domain.size) return None
    Some(domain)
  }
}
