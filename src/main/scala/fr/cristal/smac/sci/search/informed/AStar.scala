// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.informed

import fr.cristal.smac.sci.search._


/**
  * A* evaluates nodes by combining g(n),
  * the cost to reach the node, and h(n), the cost
  * to get from the node to the goal:
  */
trait AStar[State, Action]  extends FirstSearch[State, Action]{

  override def newFrontier(problem: InformedProblem[State, Action], state: State, noAction: Action): PriorityFrontier[State, Action, Node] = {
    val costNodeOrdering: Ordering[Node] = (n1: Node, n2: Node) => Ordering[Double].reverse.compare(n1.fValue, n2.fValue)
    new PriorityFrontier[State, Action, Node](
      HeuristicsNode(
        state,
        noAction,
        None,
        0.0,
        problem.h(state).toDouble,
        problem.h(state).toDouble),
      costNodeOrdering)
  }

  override def develop(problem: InformedProblem[State, Action], parent: Node, action: Action): Node = {
    val childState = problem.outcome(action, parent.state)
    val g : Double = parent.gValue +  problem.cost(parent.state, action).toDouble
    val h : Double = problem.h(childState).toDouble
    HeuristicsNode(
      state = childState,
      parent = Some(parent),
      action = action,
      gValue = g,
      hValue = h,
      fValue = g + h
    )
  }
}