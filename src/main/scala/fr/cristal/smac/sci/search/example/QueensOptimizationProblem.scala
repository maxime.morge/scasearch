// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.example

import fr.cristal.smac.sci.search.OptimizationProblem
import fr.cristal.smac.sci.search.example.Queen._
import fr.cristal.smac.sci.utils.Cost

import scala.util.Random

object Queen {
  type Queen = (Int, Int)
  type Configuration = List[Queen]
  type Move = (Queen, Queen)

  def show(configuration: Configuration): Unit = {
    for (row <- configuration.size to 1 by -1){
      for (column <- 1 to configuration.size){
       if (configuration.contains((row,column))) print("O")
       else print("X")
      }
      print("\n")
    }
  }
}

/**
 * Class representing a n-queens problem
 */
class QueensOptimizationProblem(val n : Int) extends OptimizationProblem[Configuration, Move]{

  /**
   * Returns a random initial state
   */
  override def initialState: Configuration =
    (for (column <- 1 to n) yield (column, Random.nextInt(n) + 1)).toList

  /**
   * Returns the legal actions in a state
   */
  override def actions(state: Configuration): List[Move] =
    for(queen <- state; row <- 1 to n  if row != queen._2)
      yield (queen, (queen._1, row))

  /**
   * Returns the outcome of an action in a state
   */
  override def outcome(action: Move, state: Configuration): Configuration = {
    action._2 :: state.filter(_ != action._1)
  }

  /**
   * Returns the value of the objective function for a state
   */
  override def objective(state: Configuration): Cost.Cost =
    (for(q1 <- state; q2 <- state if q1._1 != q2._2 && isAttacked(q1, q2))
    yield 1).sum/2

  /**
   * Returns true of the queens q1 and q2 attack each other
   * false otherwise
   */
  def isAttacked(q1: Queen, q2: Queen) : Boolean = // q1 and q2 attacks each other if
      q1._2 == q2._2 || //  they are in the same column
      (q2._1-q1._1).abs == (q2._2-q1._2).abs // or in the same diagonal

}
