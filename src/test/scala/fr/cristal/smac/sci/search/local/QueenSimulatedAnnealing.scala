// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.local

import fr.cristal.smac.sci.search.example.Queen._
import fr.cristal.smac.sci.search.example.QueensOptimizationProblem
import fr.cristal.smac.sci.utils.MathUtils._

import org.scalatest.flatspec._
import org.scalatest.matchers._

/**
 * Simulated annealing for searching a position for queens
 */
class QueenSimulatedAnnealing extends AnyFlatSpec with should.Matchers {
  "The position of queens " should "be a localMinima" in {
    val pb = new QueensOptimizationProblem(8) with SimulatedAnnealingSearch[Configuration, Move] {
      override def initialTemperature: Double = 100.0
      override def threshold: Double = 0.001
      override def decayRate: Double = .9999
    }
    //show(csp.initialState)
    val localMinima = pb.search(pb)
    //show(localMinima)
    //println(csp.objective(localMinima).toDouble)
    for (neighbor <-  pb.actions(localMinima).map(a => pb.outcome(a,localMinima)))
      assert( pb.objective(localMinima) <= pb.objective(neighbor) )
  }
}