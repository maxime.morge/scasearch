// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.informed

import fr.cristal.smac.sci.search.example.Itinerary._
import fr.cristal.smac.sci.search.example.ItineraryInformedProblem
import fr.cristal.smac.sci.search.{Frontier, HeuristicsNode, Problem}
import org.scalatest.flatspec._
import org.scalatest.matchers._

/**
 *  Depth-first search (DFS) algorithm for searching an itinerary
 */
class ItineraryAStarSearch extends AnyFlatSpec with should.Matchers {
  "The itinerary from Arad to Bucharest" should "be through Sibiu and Fagaras" in {
    val pb = new ItineraryInformedProblem(In(Arad), In(Bucharest)) with AStar[In, Move] {
      override def newFrontier(state: In, noAction: Move): Frontier[In, Move, HeuristicsNode[In, Move]] = ???
      override def develop(problem: Problem[In, Move], parent: HeuristicsNode[In, Move], action: Move): HeuristicsNode[In, Move] = ???
    }
    assert(pb.search(pb, Stay) == List(
      Goto(Sibiu),
      Goto(RimnicuVilcea),
      Goto(Pitesti),
      Goto(Bucharest)
    )
    )
  }
}