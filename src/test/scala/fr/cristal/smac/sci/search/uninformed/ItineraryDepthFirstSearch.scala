// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.uninformed

import fr.cristal.smac.sci.search.example.Itinerary._
import fr.cristal.smac.sci.search.example.ItineraryProblem
import org.scalatest.flatspec._
import org.scalatest.matchers._

/**
 *  Depth-first search (DFS) algorithm for searching an itinerary
 */
class ItineraryDepthFirstSearch extends AnyFlatSpec with should.Matchers {
  "The itinerary from Arad to Bucharest" should "be through Sibiu and Fagaras" in {
    val pb = new ItineraryProblem(In(Arad), In(Bucharest)) with DepthFirstSearch[In, Move]
    assert(pb.search(pb, Stay) == List(
      Goto(Sibiu),
      Goto(Fagaras),
      Goto(Bucharest)
    )
    )
  }
}