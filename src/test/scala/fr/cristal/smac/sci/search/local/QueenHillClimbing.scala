// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.local

import fr.cristal.smac.sci.search.example.Queen._
import fr.cristal.smac.sci.search.example.QueensOptimizationProblem
import org.scalatest.flatspec._
import org.scalatest.matchers._

/**
 * HillClimbing for searching a position for queens
 */
class QueenHillClimbing extends AnyFlatSpec with should.Matchers {
  "The position of queens " should "be " in {
    val pb = new QueensOptimizationProblem(8) with HillClimbing[Configuration, Move]
    //show(csp.initialState)
    val localMinima = pb.search(pb)
    //show(localMinima)
    //println(csp.objective(localMinima).toDouble)
    for (neighbor <-  pb.actions(localMinima).map(a => pb.outcome(a,localMinima)))
      assert( pb.objective(localMinima) <= pb.objective(neighbor) )
  }
}