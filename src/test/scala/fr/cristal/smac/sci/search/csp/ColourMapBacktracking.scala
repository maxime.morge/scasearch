// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.csp

import fr.cristal.smac.sci.search.example.ColouringMap._

import org.scalatest.flatspec._
import org.scalatest.matchers._

/**
 * Backtracking algorithm for colouring a map
 */
class ColourMapBacktracking extends AnyFlatSpec with should.Matchers {
  "The colours of territories" should "be fully specified such that no neighboring regions have the same color." in {
    val solver = new Backtracking
    val solution = solver.search(csp)
    //println(solution)
    assert(csp.isFull(solution.get) && solution.get.areSatisfied(csp.constraints))
  }
}
