// Copyright (C) Maxime MORGE 2021
package fr.cristal.smac.sci.search.adversial

import fr.cristal.smac.sci.search.ZeroSumGame
import fr.cristal.smac.sci.search.example.TicTacToeGame

import org.scalatest.flatspec._
import org.scalatest.matchers._

/**
 * Minimax algorithm for ticktacktoe
 */
class TicTacToeMinMax  extends AnyFlatSpec with should.Matchers {
  "The ticktacktoe" should "be solved by the minimax algorithm" in {
    val solver = new MinMax(TicTacToeGame)
    val board = Array.ofDim[Char](3, 3)
    board(2)(2) = TicTacToeGame.X
    board(1)(1) = TicTacToeGame.O
    val state = (ZeroSumGame.Max,board)
    val action = solver.minMaxDecision(state)
    val bestActions = List((1,0),(2,0),(0,1),(2,1),(0,2),(1,2),(0,0))
    assert(bestActions.contains(action))
  }
}
